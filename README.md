<!-- TOC -->

- [React Testing Workshop by Kent C. Dobbs](#react-testing-workshop-by-kent-c-dobbs)
  - [Checklist](#checklist)
  - [Notes](#notes)

<!-- /TOC -->

# React Testing Workshop by Kent C. Dobbs

Code follow along to Kent C Dobbs' FEM React Testing Workshop

## Checklist

- [x] ~~_Lesson 01: Overview_~~ [2020-02-28]
- [x] ~~_Lesson 02: Testing Fundamentals_~~ [2020-02-28]

## Notes
